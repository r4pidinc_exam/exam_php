<?php

/*
TECHNICAL TEST

PURPOSE: It is to test your understanding on object oriented programming
WHAT TO DO: Normalize the code as much as possible so that it is scalable in the future if there were more to add on for similar functions
HINT: U may need to normalize a few iteration
*/
public function prepare_markets()
{
    $match_summary = $match['pbp_stats']['match_summary'];

    $this->settle_map_draw_first_blood($match_summary);
    $this->settle_map_first_tower_destroyed_location($match_summary);
    $this->settle_map_first_inhibitor_destroyed_location($match_summary);

    $this->settle_map_total_gold_diff($match_summary);
    $this->settle_map_which_team_has_highest_gold($match_summary);
    $this->settle_map_team_with_most_assist_point($match_summary);

    $this->settle_map_total_dragon_slain_hdp($match_summary);
    $this->settle_map_total_dragon_slain_over_under($match_summary);
    $this->settle_map_total_dragon_slain_odd_even($match_summary);
    $this->settle_map_total_baron_slain_hdp($match_summary);
    $this->settle_map_total_baron_slain_over_under($match_summary);
    $this->settle_map_total_baron_slain_odd_even($match_summary);

    $this->settle_map_destroy_first_tower($match_summary);
    $this->settle_map_to_kill_first_baron($match_summary);
    $this->settle_map_to_kill_first_rift_herald($match_summary);
}
/* ==============================*/
private function settle_map_draw_first_blood($match_summary)
{
    $player_id = $match_summary['firsts']['first_blood']['player_id'];
    if (!$player_id)
    {
        return;
    }

    $team_id = $this->team['home']['id'];
    if (!$this->is_player_home_team($player_id))
    {
        $team_id = $this->team['away']['id'];
    }

    $this->result["{$this->mapNumStr} TEAM TO DRAW FIRST BLOOD"] = $team_id;
}

private function settle_map_first_tower_destroyed_location($match_summary)
{
    $tower_location = $match_summary['objective_events']['towers'][0]['lane'];
    if (!$tower_location)
    {
        return;
    }

    $team_id = $this->team['home']['id'];
    if (!$this->is_player_home_team($player_id))
    {
        $team_id = $this->team['away']['id'];
    }

    $this->result["{$this->mapNumStr} FIRST TOWER DESTROYED LOCATION"] = $team_id;
}

private function settle_map_first_inhibitor_destroyed_location($match_summary)
{
    $inhibitor_location = $match_summary['objective_events']['inhibitors'][0]['lane'];
    if (!$inhibitor_location)
    {
        return;
    }

    $team_id = $this->team['home']['id'];
    if (!$this->is_player_home_team($player_id))
    {
        $team_id = $this->team['away']['id'];
    }

    $this->result["{$this->mapNumStr} FIRST INHIBITOR DESTROYED LOCATION"] = $team_id;
}

/*=================*/

private function settle_map_total_gold_diff($match_summary)
{
    $blue_roster_player_id = $match_summary['blue_roster']['players'][0]['player_id'];

    if (!$blue_roster_player_id)
    {
        return;
    }

    list($home, $away) = $this->get_home_away_team($blue_roster_player_id);

    $home_gold_earned = array_sum(
        array_column($match_summary[$home]['players'], 'gold_earned')
    );

    $away_gold_earned = array_sum(
        array_column($match_summary[$away]['players'], 'gold_earned')
    );

    $total_gold_diff = abs(round(($home_gold_earned - $away_gold_earned) / 1000));
    $this->result["{$this->mapNumStr} TOTAL GOLD DIFFERENCE OVER/UNDER (IN THOUSANDS)"] = $total_gold_diff;
}

private function settle_map_which_team_has_highest_gold($match_summary)
{
    $blue_roster_player_id = $match_summary['blue_roster']['players'][0]['player_id'];

    if (!$blue_roster_player_id)
    {
        return;
    }

    list($home, $away) = $this->get_home_away_team($blue_roster_player_id);

    $home_gold_earned = array_sum(
        array_column($match_summary[$home]['players'], 'gold_earned')
    );
    $away_gold_earned = array_sum(
        array_column($match_summary[$away]['players'], 'gold_earned')
    );

    $team_id = $this->team['home']['id'];
    if ($away_gold_earned > $home_gold_earned)
    {
        $team_id = $this->team['away']['id'];
    }

    $this->result["{$this->mapNumStr} WHICH TEAM HAVE HIGHEST GOLD"] = $team_id;
}

private function settle_map_team_with_most_assist_point($match_summary)
{
    $blue_roster_player_id = $match_summary['blue_roster']['players'][0]['player_id'];

    if (!$blue_roster_player_id)
    {
        return;
    }

    list($home, $away) = $this->get_home_away_team($blue_roster_player_id);

    $home_assists = array_sum(
        array_column($match_summary[$home]['players'], 'assists')
    );
    $away_assists = array_sum(
        array_column($match_summary[$away]['players'], 'assists')
    );

    $team_id = $this->team['home']['id'];
    if ($away_assists > $home_assists)
    {
        $team_id = $this->team['away']['id'];
    }

    $this->result["{$this->mapNumStr} TEAM WITH MOST ASSIST POINT"] = $team_id;
}
/* ============================ */

private function settle_map_total_dragon_slain_hdp($match_summary)
{
    $events = $match_summary['objective_events']['dragons'];
    $scoreline = $this->common_event_score_line($events);
    $this->result["{$this->mapNumStr} TOTAL DRAGON SLAIN HANDICAP"] = $scoreline;
}

private function settle_map_total_dragon_slain_over_under($match_summary)
{
    $events = $match_summary['objective_events']['dragons'];
    $scoreline = $this->common_event_score_line($events);
    $this->result["{$this->mapNumStr} TOTAL DRAGON SLAIN OVER/UNDER"] = $scoreline;
}

private function settle_map_total_dragon_slain_odd_even($match_summary)
{
    $events = $match_summary['objective_events']['dragons'];
    $scoreline = $this->common_event_score_line($events);
    $this->result["{$this->mapNumStr} TOTAL DRAGON SLAIN ODD/EVEN"] = $scoreline;
}

private function settle_map_total_baron_slain_hdp($match_summary)
{
    $events = $match_summary['objective_events']['barons'];
    $scoreline = $this->common_event_score_line($events);
    $this->result["{$this->mapNumStr} TOTAL BARON SLAIN HANDICAP"] = $scoreline;
}

private function settle_map_total_baron_slain_over_under($match_summary)
{
    $events = $match_summary['objective_events']['barons'];
    $scoreline = $this->common_event_score_line($events);
    $this->result["{$this->mapNumStr} TOTAL BARON SLAIN OVER/UNDER"] = $scoreline;
}

private function settle_map_total_baron_slain_odd_even($match_summary)
{
    $events = $match_summary['objective_events']['barons'];
    $scoreline = $this->common_event_score_line($events);
    $this->result["{$this->mapNumStr} TOTAL BARON SLAIN ODD/EVEN"] = $scoreline;
}

/*==================================*/


private function settle_map_to_kill_first_rift_herald($match_summary)
{
    $playe_id = $match_summary['firsts']['first_rift_herald']['player_id'];
    if (!$playe_id)
    {
        return;
    }

    $team_id = $this->team['home']['id'];
    if (!$this->is_player_home_team($playe_id))
    {
        $team_id = $this->team['away']['id'];
    }

    $this->result["{$this->mapNumStr} TO KILL FIRST RIFT HERALD"] = $team_id;
}

private function settle_map_destroy_first_tower($match_summary)
{
    $playe_id = $match_summary['firsts']['first_tower']['player_id'];
    if (!$playe_id)
    {
        return;
    }

    $team_id = $this->team['home']['id'];
    if (!$this->is_player_home_team($playe_id))
    {
        $team_id = $this->team['away']['id'];
    }

    $this->result["{$this->mapNumStr} DESTROY FIRST TOWER"] = $team_id;
}

private function settle_map_to_kill_first_baron($match_summary)
{
    $playe_id = $match_summary['firsts']['first_baron']['player_id'];
    if (!$playe_id)
    {
        return;
    }

    $team_id = $this->team['home']['id'];
    if (!$this->is_player_home_team($playe_id))
    {
        $team_id = $this->team['away']['id'];
    }

    $this->result["{$this->mapNumStr} TO KILL FIRST BARON"] = $team_id;
}

    
/* ========================================= */
